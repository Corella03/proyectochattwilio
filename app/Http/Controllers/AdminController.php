<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;

class AdminController extends Controller
{
    /**
     * return Show admin view and how many channels exist.
     */
    public function index()
    {
        $sId = 'ACdfcfe91d0088749a2eaac86218668f94';    
        $authToken = 'f9bbb2a4bbeaba9836a2defa721bf21e';
        $admin = new Client($sId, $authToken);
        $services = $admin->chat->v2->services->read();
        $channels = $admin->chat->v2->services($services[0]->sid)
                                    ->channels
                                    ->read();
        $amount = count($channels);
        return view('/admin/index',compact('amount'));
    }
}
