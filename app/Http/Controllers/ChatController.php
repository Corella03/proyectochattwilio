<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Redirect;
use Session;

class ChatController extends Controller
{
    protected $sId;
    protected $authToken;
    protected $admin;
    protected $services;

    public function __construct()
    {
        $this->sId = 'ACdfcfe91d0088749a2eaac86218668f94';    
        $this->authToken = 'f9bbb2a4bbeaba9836a2defa721bf21e';
        $this->admin = new Client($this->sId, $this->authToken);
        $this->services = $this->admin->chat->v2->services->read();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Session::get('members');
        $member = Session::get('member');
        $id = Session::get('id');
        
        return view('/user/chat',compact('members'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $id = Session::get('id');
        $member = Session::get('member');
        $message = $this->admin->chat->v2->services($this->services[0]->sid)
                                    ->channels($id)
                                    ->messages
                                    ->create(array("body" => $request->message, "from" => $member->identity));
        //dd("Exploteeeeeeeeeeee");
        return $this->index();
    }

    public function refresh()
    {
        $id = Session::get('id');
        $messages = $this->admin->chat->v2->services($this->services[0]->sid)
                                                ->channels($id)
                                                ->messages
                                                ->read();
        if(!$messages){
            $messages == "";
        }
        $member = Session::get('member');
        $user = $member->identity;
        return view('/user/refresh_messages', compact('messages', 'user'));
    }
   
}
