<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Redirect;
use Session;

class UserController extends Controller
{
    protected $sId;
    protected $authToken;
    protected $admin;
    protected $services;

    public function __construct()
    {
        $this->sId = 'ACdfcfe91d0088749a2eaac86218668f94';    
        $this->authToken = 'f9bbb2a4bbeaba9836a2defa721bf21e';
        $this->admin = new Client($this->sId, $this->authToken);
        $this->services = $this->admin->chat->v2->services->read();
    }
    
    /**
     * return Show the form user and shows all existing channels.
     * 
     */
    public function index()
    {
        $channels = $this->admin->chat->v2->services($this->services[0]->sid)
                                    ->channels
                                    ->read();
        //dd($channels);
        return view('/user/index',compact('channels'));
    }

    /**
     * return Show the form from register users
     */
    public function view_register($id)
    {
        return view('user/login', compact("id"));
    }
    
    /**
     * Create new member in a channel.
     * return member registered
     */
    public function save($id, $nickname)
    {
        $member = $this->admin->chat->v2->services($this->services[0]->sid)
                           ->channels($id)
                           ->members
                           ->create($nickname);
        return $member;
    }

    /**
     * save in session necessary data for chat.
     */
    public function save_in_session($members, $member, $id)
    {
        Session::put('members', $members);
        Session::put('member', $member);
        Session::put('id', $id);
    }

    /**
     * Register only if is new.
     * Refresh list of members.
     */
    public function register_if_new_user($channel_id, $name)
    {
        $mem = $this->save($channel_id, $name);
        $members = $this->admin->chat->v2->services($this->services[0]->sid)->channels($channel_id)->members->read();
        $this->save_in_session($members, $mem, $channel_id);
    }

    /**
     * Create a new user with the request data in the request.
     *
     * @param  \Illuminate\Http\Request  $request identity and channel_id
     * @return \Illuminate\Http\Response redirect form chat.
     */
    public function store(Request $request, $id)
    {
        $members = $this->admin->chat->v2->services($this->services[0]->sid)->channels($id)->members->read();
        if($members)
        {
            foreach ($members as $member) {
                if ($member->identity == $request->nickname)
                {
                    $this->save_in_session($members, $member, $id);
                    return Redirect::to('/user/chat/');
                }
            }
            $this->register_if_new_user($id, $request->nickname);
            return Redirect::to('/user/chat/');
        }else{
            $this->register_if_new_user($id, $request->nickname);
            return Redirect::to('/user/chat/');
        }
    }
}
