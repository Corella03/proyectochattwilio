<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Redirect;


class ChannelController extends Controller
{
    protected $sId;
    protected $authToken;
    protected $admin;
    protected $services;

    public function __construct()
    {
        $this->sId = 'ACdfcfe91d0088749a2eaac86218668f94';    
        $this->authToken = 'f9bbb2a4bbeaba9836a2defa721bf21e';
        $this->admin = new Client($this->sId, $this->authToken);
        $this->services = $this->admin->chat->v2->services->read();
    }

    /**
     * return view for create a new channel.
     */
    public function index()
    {
        return view('/channel/create');
    }

     /**
     * Create a new channel with the request data in the created service.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $channel = $this->admin->chat->v2->services($this->services[0]->sid)
                                    ->channels
                                    ->create(array("friendlyName" => $request->name_channel));
        return Redirect::to('/administrator');
    }

    /**
     * Get all the channels created to show them
     *
     * @return \Illuminate\Http\Response View channel show with the all channels.
     */
    public function show()
    {
        $channels = $this->admin->chat->v2->services($this->services[0]->sid)
                                    ->channels
                                    ->read();
        return view('/channel/show', compact('channels'));
    }
    
    /**
     * Show the form for editing the specified channel.
     *
     * @param  int  $id id of channel.
     * @return \Illuminate\Http\Response view channel edit with id for editing.
     */
    public function edit($id)
    {
        return view('/channel/edit', compact('id'));
    }

    /**
     * Update the specified channel.
     *
     * @param  \Illuminate\Http\Request  $request new information for update the channel.
     * @param  int  $id id of the channel to modify.
     * @return \Illuminate\Http\Response View with all channels.
     */
    public function update(Request $request, $id)
    {
        $channel = $this->admin->chat->v2->services($this->services[0]->sid)
                                    ->channels($id)
                                    ->update(array(
                                                "friendlyName" => $request->name_channel));
        return Redirect::to('/administrator/channel/show');
    }

    /**
     * Remove the specified resource from service.
     *
     * @param  int  $id id of the channel to delete
     * @return \Illuminate\Http\Response view all channels except the eliminated.
     */
    public function destroy($id)
    {
        $this->admin->chat->v2->services($this->services[0]->sid)
                        ->channels($id)
                        ->delete();
        return Redirect::to('/administrator/channel/show');
    }
}
