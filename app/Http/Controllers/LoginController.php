<?php

namespace App\Http\Controllers;
use Twilio\Rest\Client;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    protected $sId;
    protected $authToken;

    public function __construct()
    {
        $this->sId = 'ACdfcfe91d0088749a2eaac86218668f94';    
        $this->authToken = 'f9bbb2a4bbeaba9836a2defa721bf21e';
    }
   
    /**
     * show login for admin.
     */
    public function index()
    {
        $log_admin = false;
        return view('/login/index', compact('log_admin'));
    }

    /**
     * Sgow login for users.
     */
    public function admin()
    {
        $log_admin = true;
        return view('/login/index', compact('log_admin'));
    }

    /**
     * Check if password and email are correct. 
     * Check if exist a services, if no create a new.
     * 
     * @param  \Illuminate\Http\Request  $request (email and password)
     */
    public function login(Request $request)
    {
        if ($request->email == 'admin' && $request->password == 'admin') {

            $admin = new Client($this->sId, $this->authToken);
            $services = $admin->chat->v2->services->read();
            //dd($services);
            if (!$services) {
                $service = $admin->chat->v2->services->create("API_chat");
            }
            return redirect('/administrator');
                 
        }
    }
}
