@extends('layouts.app')
@include('layouts.menu')
@section('content')
<div class="centrar">
    <div class="centrar_titulo">
        <h2>Edit channel</h2>
    </div> 
    <div class="tab-content">
        <form action="{{ action('ChannelController@update', $id) }}"  method="POST">
            @csrf
            @method('PUT')   
            <div class="form-row">
                <div class="col-md-6">
                    <label><b>SID of channel</b></label>
                    <div class="input-group-prepend">
                        <label>{{$id}}</label>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-row">
                <div class="col-md-6">
                    <label><b>New name of channel</b></label>
                    <div class="input-group-prepend">
                        <input id="name_channel" type="text" class="form-control" name="name_channel" 
                            required placeholder="New name of channel">
                    </div>
                </div>
            </div>
            <br>
            <div class="centrar_botone_channel">
                <button class="btn btn-success" type="submit">Edit</button>
                <a class="btn btn-danger" type="submit" href="/administrator">Lobby</a>
            </div>
        </form>
    </div>
</div>


@endsection