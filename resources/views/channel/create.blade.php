@extends('layouts.app')
@include('layouts.menu')

@section('content')
<div class="centrar">
    <div class="centrar_titulo">
        <h2>Create new chat</h2>
    </div> 
    <div class="tab-content">
        <form method="POST">
            @csrf
            <div class="form-row">
                <div class="col-md-6">
                    <label><b>Name of channel</b></label>
                    <div class="input-group-prepend">
                        <input id="name_channel" type="text" class="form-control{{ $errors->has('name_channel') ? ' is-invalid' : '' }}"
                            name="name_channel" value="{{ old('name_channel') }}" required placeholder="Name of channel">
                        @if ($errors->has('name_channel'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name_channel') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <br>
            <div class="centrar_botone_channel">
                <button type="submit" class="btn btn-success">Create</button>
                <a class="btn btn-danger" type="submit" href="/administrator">Lobby</a>
            </div>
        </form>
    </div>
</div>
@endsection
