@extends('layouts.app')
@include('layouts.menu')

@section('content')
<div class="container">
    <div class="title_channels">Options Channels</div>
    <table class="table table-striped table-bordered">
        <tr>
        <th>SID</th>
        <th>Name</th>
        <th class="text-center">
            <a href="{{ url('/administrator') }}" class="btn btn-primary">Lobby</a>
        </th>
    </tr>
    @foreach($channels as $channel)
    <tr>
        <td> {{ $channel->sid }} </td>
        <td> {{ $channel->friendlyName }} </td>
        <td>
            <div class="btn-group" role="group">
                <form action="{{ action('ChannelController@destroy', $channel->sid) }}"  method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit">Delete</button>
                    <a href="{{ action('ChannelController@edit', $channel->sid) }}" class="btn btn-warning">Edit</a>
                </form>
                
            </div>
        </td>
        </tr>
        @endforeach
    </table>
</div>
@endsection
