@extends('layouts.app')
@include('layouts.menu_user')

@section('content')
<div class="container">
    <div class="row">
        @foreach($channels as $channel)
        <div class="col-3">
            <img src="{{ asset('img/chat.jpg') }}" alt="" width="100%" height="20%">
            <h4>{{$channel->friendlyName}}</h4>
            <div class="center_button_channel_user">
                <a href="{{ url('user/login', $channel->sid) }}" class="btn btn-success">Join</a>
            </div>
        </div>
        @endforeach
    </div>
</div>

@endsection
