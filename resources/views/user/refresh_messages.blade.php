@if($messages != "")
    @foreach ($messages as $message)
    <div class="row">
        @if ($message->from != $user)
        <div class="users_messages">
            <p>{{$message->from}}</p>
            <p>{{date_format($message->dateCreated, 'Y/d/m g:i:s')}}</p>
            <p>{{$message->body}}</p>
        </div>
        @else
        <div class="my_messages">
            <p>{{$message->from}}</p>
            <p>{{date_format($message->dateCreated, 'Y/d/m g:i:s')}}</p>
            <p>{{$message->body}}</p>
        </div>
        @endif
    </div>
        
    @endforeach
@endif