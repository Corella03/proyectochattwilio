@extends('layouts.app')
@include('layouts.menu_user')

@section('content')
<div class="centrar">
    <div class="centrar_titulo">
        <h2>Write your nickname</h2>
    </div> 
    <div class="tab-content">
        <form method="POST">
            @csrf
            <div class="form-row">
                <div class="col-md-6">
                    <label><b>Nickname</b></label>
                    <div class="input-group-prepend">
                        <input id="nickname" type="text" class="form-control" name="nickname" 
                            required placeholder="Nickname">
                    </div>
                </div>
            </div>
            <br>
            <div class="centrar_botone_channel">
                <button type="submit" class="btn btn-success">Login</button>
                <a class="btn btn-danger" type="submit" href="/user">Lobby</a>
            </div>
        </form>
    </div>
</div>

@endsection