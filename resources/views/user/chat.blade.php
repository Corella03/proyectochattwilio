@extends('layouts.app')
@include('layouts.menu_user')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-8">
        <div class="messages">
            <div class="text" style="overflow-y: auto">
                <div id="show_message">
                
                </div>
            </div>
            <div class="send_message">
                <form method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="margin_text_area">
                                <textarea name="message" id="" cols="60" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-4" >
                            <div class="center_chat_button">
                                <button class="btn btn-success" type="submit" style="height:50%; width:70%">send</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="user_chat">
            <h4>Users</h4>
            @foreach ($members as $member) 
                <div class="users_view">
                    <label> {{ $member->identity }} </label>
                    <label> {{ $member->sid }} </label>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
<script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        setInterval(
            function() {
                $('#show_message').load('refresh_messages');
            }, 1500
        );
    });
</script>