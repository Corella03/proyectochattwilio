@extends('layouts.app')

@section('content')
<div class="centrar">
    <div class="centrar_titulo">
        <h2>Chat of Twillio</h2>
    </div>

    <div class="ui-indigo login-page application navless qa-login-page">
        <div class="container navless-container">
            <div class="col-sm-5 new-session-forms-container">
                <ul class="nav-links new-session-tabs nav-tabs nav" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link qa-register-tab"  href="<?php echo e(url('/login'));?>">User</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link qa-register-tab"  href="<?php echo e(url('/login/admin'));?>">Admin</a>

                    </li>
                </ul>

                <div class="tab-content">
                    @if($log_admin)
                        <div class="login-box tab-pane active" id="login-pane" role="tabpanel">
                            <div class="login-body">
                                <form method="POST">
                                    @csrf
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <label> <b>Email</b> </label>
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">@</div>
                                                    <input id="email" type="text" class="form-control"
                                                    name="email" value="{{ old('email') }}" required placeholder="Email">
                                                <br>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <label><b>Password</b></label>
                                            <input id="password" type="password" class="form-control"
                                                name="password" value="{{ old('password') }}" required placeholder="Password">
                                            
                                        </div>
                                    </div>
                                    <br>
                                    <div class="centrar_botones">
                                        <button type="submit" class="btn btn-success">Login</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    @else
                        <div class="login-box tab-pane active" id="login-pane" role="tabpanel">
                            <div class="login-body">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <h4><b>Press the "Login" button to enter the chat room</b></h4>
                                    </div>
                                </div>
                                <br>
                                <div class="centrar_botones">
                                    <a class="btn btn-success" href="{{url('/user')}}">Login</a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection