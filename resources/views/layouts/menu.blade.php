<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
<label class="navbar-brand">Channels</label>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="{{url('/administrator/channel')}}">Create</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/administrator/channel/show')}}">Edit</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/administrator/channel/show')}}">Show</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/administrator/channel/show')}}">Delete</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a class="btn btn-outline-success my-2 my-sm-0" type="submit"  href="/login">LogOut</a>
        </form>
    </div>
</nav>
