<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
<label class="navbar-brand">Options</label>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

            <li class="nav-item">
                <a class="nav-link" href="{{url('/user')}}">Lobby</a>
            </li>
            
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a class="btn btn-outline-success my-2 my-sm-0" type="submit"  href="/login">LogOut</a>
        </form>
    </div>
</nav>