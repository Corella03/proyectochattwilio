@extends('layouts.app')
@include('layouts.menu')

@section('content')
<div class="container">
    <div class="centrar_titulo_lobby">
        <h2>Statistics of Administrator</h2>
    </div>
    <br>
    <table class="table table-bordered table-striped mt-5">
        <tr>
            <th scope="col" width="50%">
                <h5>Amount of channels created</h5>
            </th>
            <td width="50%">
                <p class="lead"> {{ $amount }} </p>
            </td>
        </tr>
    </table>
</div>
@endsection
