<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//------------------------Login------------------------
Route::get('/', 'LoginController@index');
Route::get('/login', 'LoginController@index');
Route::get('/login/admin', 'LoginController@admin');
Route::post('/login/admin', 'LoginController@login');

//------------------------Administrator------------------------
Route::get('/administrator', 'AdminController@index');

//------------------------Channels------------------------
Route::get('/administrator/channel', 'ChannelController@index');
Route::post('/administrator/channel', 'ChannelController@store');
Route::get('/administrator/channel/edit/{id}', 'ChannelController@edit');
Route::put('/administrator/channel/edit{id}', 'ChannelController@update');
Route::get('/administrator/channel/show', 'ChannelController@show');
Route::delete('/administrator/channel/delete/{id}', 'ChannelController@destroy');

//------------------------Users------------------------   
Route::get('/user', 'UserController@index');
Route::get('/user/login/{id}', 'UserController@view_register');
Route::post('/user/login/{id}', 'UserController@store');

//------------------------Users------------------------   
Route::get('/user/chat', 'ChatController@index');
Route::post('/user/chat', 'ChatController@store');

//------------------------Refresh------------------------ 
Route::get('/user/refresh_messages', 'ChatController@refresh');


